const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const next = document.querySelector("#next");
const next2 = document.querySelector("#next2");
const back2 = document.querySelector("#back2");
const back3 = document.querySelector("#back3");

const container = document.querySelector(".container");

sign_up_btn.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
});

sign_in_btn.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
});
next.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
  document.getElementById("content1").style.visibility = "hidden";
  document.getElementById("content2").style.visibility = "visible";
  document.getElementById("img-content1").style.visibility = "hidden";
  document.getElementById("img-content3").style.visibility = "visible";

  console.log("click");
});
back2.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
  setTimeout(function (){
    document.getElementById("content1").style.visibility = "visible";
    document.getElementById("content2").style.visibility = "hidden";
    document.getElementById("img-content1").style.visibility = "visible";
    document.getElementById("img-content3").style.visibility = "hidden";
  },2000);

  console.log("click");
});
next2.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
  document.getElementById("content3").style.visibility = "hidden";
  document.getElementById("content4").style.visibility = "visible";
  document.getElementById("img-content2").style.visibility = "hidden";
  document.getElementById("img-content4").style.visibility = "visible";


  console.log("click");
});
back3.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
  setTimeout(function (){
    document.getElementById("content1").style.visibility = "hidden";
    document.getElementById("content4").style.visibility = "hidden";
    document.getElementById("content3").style.visibility = "visible";
    document.getElementById("content2").style.visibility = "visible";

    document.getElementById("img-content1").style.visibility = "hidden";
    document.getElementById("img-content3").style.visibility = "visible";
    document.getElementById("img-content2").style.visibility = "visible";
    document.getElementById("img-content4").style.visibility = "hidden";
  },2000);


  console.log("click");
});